package u03

import u02.Optionals.Option._

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    @scala.annotation.tailrec
    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, tail) if n > 0 => drop(tail, n - 1)
      case _ => l
    }

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(head, tail) => append(f(head), flatMap(tail)(f))
      case _ => Nil();
    }

    import u02.Optionals._

    def max(l: List[Int]): Option[Int] = {
      @scala.annotation.tailrec
      def max_(l: List[Int], max: Option[Int]): Option[Int] = (l, max) match {
        case (Cons(head, tail), None()) => max_(tail, Some(head))
        case (Cons(head, tail), Some(v)) => max_(tail, if (head > v) Some(head) else max)
        case (Nil(), _) => max
      }
      max_(l, None())
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def mapWithFlatMap[A,B](l: List[A])(mapper: A=>B): List[B] = flatMap(l)(a => Cons(mapper(a), Nil()))

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def filterWithFlatMap[A](l1: List[A])(pred: A=>Boolean): List[A] = flatMap(l1)(
      a => if (pred(a)) Cons(a, Nil()) else Nil()
    )

    @scala.annotation.tailrec
    def foldLeft[A](l: List[A])(init: A)(merger: (A, A) => A): A = l match {
      case Cons(head, tail) => foldLeft(tail)(merger(init, head))(merger)
      case _ => init
    }

    def foldRight[A](l: List[A])(init: A)(merger: (A, A) => A): A = l match {
      case Cons(head, tail) => merger(head, foldRight(tail)(init)(merger))
      case _ => init
    }

  }
}

object ListsMain extends App {
  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  //println(List.sum(l)) // 60
  import List._
  //println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filterWithFlatMap(l)(_ >=20)) // 20,30

  println(mapWithFlatMap(l)(_ - 10))

  println(drop(l, 1)) // Cons (20 , Cons (30 , Nil ()))
  println(drop(l, 2)) // Cons (30 , Nil ())
  println(drop(l, 5)) // Nil ()

  println(flatMap(l)(v => Cons(v+1, Nil()))) // Cons (11 , Cons (21 , Cons (31 , Nil ())))
  println(flatMap(l)(v => Cons(v+1, Cons(v+2, Nil())))) // Cons (11 , Cons (12 , Cons (21 , Cons (22 , Cons (31 , Cons (32 , Nil ()))))))

  println(max ( Cons (10 , Cons (25 , Cons (20 , Nil () ) ) ) )) //Some(25)
  println(max(Nil())) //None()

  val lst = Cons (3 , Cons (7 , Cons (1 , Cons (5 , Nil () ) ) ) )
  println(foldLeft ( lst ) (0) (_ - _ ))// -16
  println(foldRight ( lst ) (0) (_ - _ )) // -8

}