package u02

import u03.Lists
import u03.Lists.List.Cons

object Modules extends App {

  // An ADT: type + module
  sealed trait Person
  object Person {

    case class Student(name: String, year: Int) extends Person
    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
    }

    import u03.Lists._
    def allCoursesBase(people: List[Person]): List[String] = List.map(
      List.filter(people)(p => p.isInstanceOf[Teacher])) {
      case Teacher(_, course) => course
    }

    def allCoursesAdvanced(people: List[Person]): List[String] = List.flatMap(people) {
      case Teacher(_, course) => Cons(course, List.Nil())
    }
  }

  println(Person.name(Person.Student("mario",2015)))

  import Person._
  import u03.Lists._
  println(name(Student("mario",2015)))

  val people: List[Person] = List.Cons(Student("Gianmarco", 2019),
    List.Cons(Teacher("Viroli", "PPS"),
      List.Cons(Teacher("Ricci", "PCD"), List.Nil())))

  println(allCoursesBase(people))
}
